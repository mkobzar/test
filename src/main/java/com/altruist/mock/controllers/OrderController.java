package com.altruist.mock.controllers;

import com.altruist.mock.services.OrderService;
import com.altruist.mock.dto.OrderDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {

  private final OrderService orderService;

  public OrderController(
          OrderService orderService) {
    this.orderService = orderService;
  }

  @PostMapping
  public OrderDto postOrder(@RequestBody() OrderDto order) {
    return orderService.postOrder(order);
  }
}
