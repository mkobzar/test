package com.altruist.mock.controllers;

import com.altruist.mock.services.ExchangeInfoService;
import com.altruist.mock.dto.ExchangeStatusDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exchange-status")
public class ExchangeStatusController {

  private final ExchangeInfoService exchangeInfoService;

  public ExchangeStatusController(
      ExchangeInfoService exchangeInfoService) {
    this.exchangeInfoService = exchangeInfoService;
  }

  @GetMapping
  public ExchangeStatusDto getExchangeStatus(@RequestParam("someData") int someData) {
    return exchangeInfoService.getExchangeStatus(someData);
  }
}
