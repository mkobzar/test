package com.altruist.mock.services;


import com.altruist.mock.dto.ExchangeStatusDto;
import org.springframework.stereotype.Service;

@Service
public class ExchangeInfoService {

  public ExchangeStatusDto getExchangeStatus(int someData) {
    var exchangeStatus = new ExchangeStatusDto();
    if(someData==0){
      exchangeStatus.setOpen(true);
      exchangeStatus.setMarketStatus(true);
      exchangeStatus.setMinsToClose(8*60);
    }
    else {
      exchangeStatus.setOpen(false);
      exchangeStatus.setMarketStatus(false);
      exchangeStatus.setMinsToClose(someData);
      exchangeStatus.setError("failed to retrieve Exchange Status");
    }
    return exchangeStatus;
  }
}
