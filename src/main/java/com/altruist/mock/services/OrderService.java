package com.altruist.mock.services;

import com.altruist.mock.dto.OrderDto;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

@Service
public class OrderService {

  public OrderDto postOrder(OrderDto order) {
    var postOrder = new OrderDto();
    if(order ==null){
      postOrder.setError("input order object should not be null");
      return postOrder;
    }
    var symbol = order.getSymbol();
    if(symbol==null || symbol.isEmpty()||symbol.isBlank()){
      postOrder.setError("symbol value should not be null");
      return postOrder;
    }

    var quantity = order.getQuantity();
    if(quantity<= 0){
      postOrder.setError("quantity must be positive integer");
      return postOrder;
    }
    var orderType = order.getType();
    if(orderType==null || orderType.isEmpty() || orderType.isBlank()){
      postOrder.setError("order type value should not be null");
      return postOrder;
    }
    orderType = orderType.toLowerCase();
    postOrder.setType(orderType);
    if(orderType.equals("market")){
      postOrder.setStatus("fll");
      var filledPrice =  ((double) new Random().nextInt(1,100000))/100.0;
      postOrder.setFilledPrice(filledPrice);
      postOrder.setClosed(new Date(System.currentTimeMillis()+84000));
    } else if(orderType.equals("limit")){
      postOrder.setStatus("new");
    } else {
      postOrder.setStatus("rej");
      postOrder.setError("unsupported order type");
      postOrder.setClosed(new Date(System.currentTimeMillis()+84000));
      return postOrder;
    }

    postOrder.setCreated(new Date(System.currentTimeMillis()));
    postOrder.setSymbol(symbol);
    postOrder.setQuantity(quantity);
    postOrder.setExchangeId("OTC");
    postOrder.setId(UUID.randomUUID());
    postOrder.setTimeInForce(order.getTimeInForce());
    postOrder.setExtraStringField1(order.getExtraStringField1());
    postOrder.setExtraStringField2(order.getExtraStringField2());
    postOrder.setPrice(order.getPrice());
    return postOrder;
  }
}
