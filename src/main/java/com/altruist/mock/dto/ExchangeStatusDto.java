package com.altruist.mock.dto;

import lombok.Data;

@Data
public class ExchangeStatusDto {

  private boolean open;
  private boolean marketStatus;
  private boolean oneHourEarlyStatus;
  private long minsToClose;
  private String error;
}
