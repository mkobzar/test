package com.altruist.mock.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Data
public class OrderDto {
    private String symbol;
    private UUID id;
    private String type;
    private String timeInForce;
    private String exchangeId;
    private String extraStringField1;
    private String extraStringField2;
    private String error;
    private String status;
    private String rejectReason;
    private Date created;
    private Date closed;
    private Date modified;
    private int quantity;
    private double price;
    private double filledPrice;
}
